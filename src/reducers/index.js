import { combineReducers } from 'redux';

import wines from './wines';

export default combineReducers({ wines });
