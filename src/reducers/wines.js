import {
    CREATE_WINE_SUCCESS,
    CREATE_WINE_FAIL,
    LIST_WINE_SUCCESS,
    LIST_WINE_FAIL,
    REMOVE_WINE_SUCCESS,
    REMOVE_WINE_FAIL,
    UPDATE_WINE_SUCCESS,
    UPDATE_WINE_FAIL
} from '../constants';

const initialState = {
    wines : []
};

export default (state = initialState, action) => {
    const wines = [];

    switch (action.type) {
        case CREATE_WINE_SUCCESS:
            return { ...state, wines: [ ...state.wines, action.payload ] };

        case CREATE_WINE_FAIL:
            return { ...state, error: action.error };

        case LIST_WINE_SUCCESS:
            return { ...state, wines: action.payload };

        case LIST_WINE_FAIL:
            return { ...state, error: action.error };

        case REMOVE_WINE_SUCCESS:
            state.wines.forEach(wine => {
                if (wine.id !== action.payload.id) {
                    wines.push(wine);
                }
            });

            return { ...state, wines };

        case REMOVE_WINE_FAIL:
            return { ...state, error: action.error };


        case UPDATE_WINE_SUCCESS:
            return {
                ...state,
                wines : state.wines.map(wine => (wine.id !== action.payload.id ? wine : action.payload))
            };

        case UPDATE_WINE_FAIL:
            return { ...state, error: action.error };

        default:
            return state;
    }
};
