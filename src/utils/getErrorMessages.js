export default (key, errorName) => {
    switch (errorName) {
        case 'REQUIRED':
            return `Field ${key} is required`;

        default:
            break;
    }
};
