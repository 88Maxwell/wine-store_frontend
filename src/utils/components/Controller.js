/* eslint-disable react/jsx-no-bind, react/no-multi-comp*/

import React from 'react';
import { Route } from 'react-router';
import nest from './nest';


// eslint-disable-next-line
export default ({ Components, ...rest })  => (
    <Route
        {...rest} render={props => nest(Components)(props)}
    />
);
