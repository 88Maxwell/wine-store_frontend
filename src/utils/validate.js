import LIVR from 'livr';
import getErrorMessages from './getErrorMessages';

export default  (data, rules) => {
    const validator = new LIVR.Validator(rules).prepare();
    const validData = validator.validate(data);

    if (validData) {
        return {
            isValid : true,
            data    : validData
        };
    }
    const errors = validator.getErrors();
    const extendedErrors = {};

    // eslint-disable-next-line
    Object.keys(errors).forEach(key => extendedErrors[key] = getErrorMessages(key, errors[key]));

    return {
        isValid : false,
        errors  : extendedErrors
    };
};

