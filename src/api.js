import request from './utils/request';

// eslint-disable-next-line
export const wine = {
    create : data => request.post('/wine', data),
    list   : data => request.post('/wines', data),
    update : data => request.patch(`/wine/${data.id}`, data),
    remove : id => request.delete(`/wine/${id}`)
};
