import * as api from '../api';
import {
    CREATE_WINE_FAIL,
    CREATE_WINE_SUCCESS,
    LIST_WINE_SUCCESS,
    LIST_WINE_FAIL,
    REMOVE_WINE_SUCCESS,
    REMOVE_WINE_FAIL,
    UPDATE_WINE_SUCCESS,
    UPDATE_WINE_FAIL
} from '../constants';

export const create = data => async dispatch => {
    const wines = await api.wine.create(data);

    try {
        dispatch({
            type    : CREATE_WINE_SUCCESS,
            payload : wines
        });
    } catch (error) {
        dispatch({
            type    : CREATE_WINE_FAIL,
            payload : error
        });
    }
};

export const list = data => async dispatch => {
    try {
        const wines = await api.wine.list(data);

        dispatch({
            type    : LIST_WINE_SUCCESS,
            payload : wines
        });
    } catch (error) {
        dispatch({
            type    : LIST_WINE_FAIL,
            payload : error
        });
    }
};

export const update = data => async dispatch => {
    const updatedWine = await api.wine.update(data);

    try {
        dispatch({
            type    : UPDATE_WINE_SUCCESS,
            payload : updatedWine
        });
    } catch (error) {
        dispatch({
            type    : UPDATE_WINE_FAIL,
            payload : error
        });
    }
};


export const remove = id => async dispatch => {
    const deletedWine = await api.wine.remove(id);

    try {
        dispatch({
            type    : REMOVE_WINE_SUCCESS,
            payload : deletedWine
        });
    } catch (error) {
        dispatch({
            type    : REMOVE_WINE_FAIL,
            payload : error
        });
    }
};

