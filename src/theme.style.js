import { createMuiTheme } from '@material-ui/core/styles';
import { green, red } from '@material-ui/core/colors';

export default createMuiTheme({
    typography : { useNextVariants: true },
    palette    : {
        primary : {
            light        : green[500],
            main         : green[700],
            dark         : green[900],
            contrastText : '#fff'
        },
        secondary : {
            light        : red[500],
            main         : red[700],
            dark         : red[900],
            contrastText : '#fff'
        }
    }
});
