import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormControl, FormHelperText, InputLabel, Select, Checkbox, MenuItem } from '@material-ui/core';

import { MenuProps } from './selectMenuProps.style';

export default class CustomSelect extends Component {
    static propTypes = {
        id           : PropTypes.string,
        elementStyle : PropTypes.string,
        required     : PropTypes.bool,
        multiple     : PropTypes.bool,
        error        : PropTypes.string,
        value        : PropTypes.any,
        label        : PropTypes.string.isRequired,
        valueList    : PropTypes.array.isRequired,
        handleChange : PropTypes.func.isRequired
    };

    state = {
        errored : false
    }

    componentWillMount() {
        const { error } = this.props;

        this.setState({ errored: !!error });
    }

    renderSelectedValue = selected => (this.props.multiple ? selected.join(', ') : selected);

    renderSelectsList = (list, checkedList) => {
        const { multiple } = this.props;

        return list.map(elem => (
            <MenuItem key={elem} value={elem}>
                {multiple ? <Checkbox checked={checkedList.indexOf(elem) > -1} /> : null}
                {elem}
            </MenuItem>
        ));
    };

    render() {
        const { id, elementStyle, multiple, label, value, valueList, error, required, handleChange } = this.props;
        const { errored } = this.state;
        const name = label.toLowerCase();

        return (
            <FormControl error={errored} required={required} className={elementStyle}>
                <InputLabel htmlFor={id}>{label}</InputLabel>
                <Select
                    multiple={multiple}
                    value={value}
                    onChange={handleChange}
                    inputProps={{ name, id }}
                    renderValue={this.renderSelectedValue}
                    MenuProps={MenuProps}
                >
                    {this.renderSelectsList(valueList, value)}
                </Select>
                {error ? <FormHelperText htmlFor={id} error={!!error}>{error}</FormHelperText> : null}

            </FormControl>
        );
    }
}
