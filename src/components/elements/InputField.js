import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormControl, FormHelperText, InputLabel, Input } from '@material-ui/core';

export default class InputField extends Component {
    static propTypes = {
        id           : PropTypes.string,
        elementStyle : PropTypes.string,
        error        : PropTypes.string,
        required     : PropTypes.bool,
        value        : PropTypes.string,
        label        : PropTypes.string.isRequired,
        handleChange : PropTypes.func.isRequired
    };

    render() {
        const { id, elementStyle, label, value, required, error, handleChange } = this.props;
        const name = label.toLowerCase();

        return (
            <FormControl error={!!error} required={required} className={elementStyle}>
                <InputLabel htmlFor={id}>{label}</InputLabel>
                <Input
                    id={id} name={name} value={value}
                    inputComponent='input' onChange={handleChange} fullWidth
                />
                {error ? <FormHelperText htmlFor={id} error={!!error}>{error}</FormHelperText> : null}
            </FormControl>
        );
    }
}
