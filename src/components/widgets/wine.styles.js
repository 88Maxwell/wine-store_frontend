export const muiStyles = theme => {
    const { unit } = theme.spacing;

    return {
        expansionSummary         : { 'padding-right': 0 },
        expansionSummaryExpanded : { margin: `${unit * 2} 0` },
        wineDataList             : { width: '100%' }
    };
};
