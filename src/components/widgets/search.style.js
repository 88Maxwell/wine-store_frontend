export const muiStyles = theme => {
    const { unit } = theme.spacing;
    const { primary } = theme.palette.text;

    return {
        paper             : { padding: unit, color: primary },
        collapse          : { 'margin-bottom': unit },
        searchFormControl : { width: '100%' },
        grid              : { padding: `0 ${unit}px` },
        formControl       : { width: '100%' },
        searchButton      : { display: 'flex' },
        button            : { margin: unit },
        chip              : { margin: unit / 2 }
    };
};
