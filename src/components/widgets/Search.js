import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
    Grid,
    Paper,
    Chip,
    Collapse,
    Button
} from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';

import Select from '../elements/Select';
import InputField from '../elements/InputField';

import * as WineActions from '../../actions/wine';
import FILTERS from '../../../configs/Wine';

import { muiStyles } from './search.style';

class Search extends Component {
    static propTypes = {
        classes : PropTypes.object.isRequired,
        list    : PropTypes.func.isRequired
    };

    state = {
        searchValue        : '',
        varietalFilterList : [],
        countryFilterList  : [],
        ageFilterList      : [],
        expanded           : false
    };

    handleChange = param => event =>  this.setState({ [param]: event.target.value });

    handleExpand = () => this.setState(state => ({ expanded: !state.expanded }));

    handleSearch = async () => {
        const { searchValue, varietalFilterList, countryFilterList, ageFilterList } = this.state;
        const { list } = this.props;

        await list({ searchValue, varietalFilterList, countryFilterList, ageFilterList });
    };


    generateChips = chipses => {
        const { classes } = this.props;

        return (
            <div>
                {chipses.map(value => (
                    <Chip
                        key={value}
                        label={value}
                        className={classes.chip}
                    />
                ))}
            </div>
        );
    }

    render() {
        const { classes } = this.props;
        const { expanded, ageFilterList, countryFilterList, varietalFilterList, searchValue } = this.state;
        const { VARIETAL, AGE, COUNTRY } = FILTERS;

        return (
            <Paper className={classes.paper}>
                <InputField
                    id='input-search'
                    elementStyle={classes.searchFormControl}
                    value={searchValue}
                    label='Search'
                    handleChange={this.handleChange('searchValue')}
                />

                <Button
                    size='small'
                    className={classes.button}
                    onClick={this.handleExpand}
                >
                    Extended search {expanded ? <ExpandLess /> : <ExpandMore />}
                </Button>

                <Collapse
                    in={expanded}
                    timeout='auto'
                    unmountOnExit
                >
                    <Grid className={classes.collapse} container>
                        <Grid className={classes.grid} item xs={4}>
                            <Select
                                multiple
                                id='select-varietal'
                                elementStyle={classes.formControl}
                                label='Varietal'
                                value={varietalFilterList}
                                valueList={VARIETAL}
                                handleChange={this.handleChange('varietalFilterList')}
                            />
                        </Grid>
                        <Grid className={classes.grid} item xs={4}>
                            <Select
                                multiple
                                id='select-age'
                                elementStyle={classes.formControl}
                                label='Age'
                                value={ageFilterList}
                                valueList={AGE}
                                handleChange={this.handleChange('ageFilterList')}
                            />
                        </Grid>
                        <Grid className={classes.grid} item xs={4}>
                            <Select
                                multiple
                                id='select-country'
                                elementStyle={classes.formControl}
                                label='Country'
                                value={countryFilterList}
                                valueList={COUNTRY}
                                handleChange={this.handleChange('countryFilterList')}
                            />
                        </Grid>
                    </Grid>
                </Collapse>

                <Grid
                    container
                    alignItems='center'
                >
                    <Grid item xs={10}>
                        {this.generateChips([ ...ageFilterList, ...countryFilterList, ...varietalFilterList ])}
                    </Grid>
                    <Grid
                        container
                        className={classes.searchButton}
                        item
                        justify='flex-end'
                        alignItems='flex-end'
                        xs={2}
                    >
                        <Button
                            variant='contained'
                            color='primary'
                            className={classes.button}
                            onClick={this.handleSearch}
                        >
                            Search
                        </Button>
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

export default connect(
    state => ({
        wines : state.wines.wines
    }),
    { ...WineActions }
)(withStyles(muiStyles)(Search));
