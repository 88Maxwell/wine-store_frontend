import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import { Divider } from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';
import { muiStyles } from './wineData.style';


const WineData = ({ keys, children, classes }) => (
    <Fragment>
        <ListItem>
            <Typography className={classes.key}>{keys}</Typography>
            <Typography className={classes.value}>{children}</Typography>
        </ListItem>
        <Divider />
    </Fragment>
);

WineData.propTypes = {
    keys     : PropTypes.string.isRequired,
    children : PropTypes.string.isRequired,
    classes  : PropTypes.object.isRequired
};

export default withStyles(muiStyles)(WineData);
