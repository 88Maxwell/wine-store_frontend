export const muiStyles = theme => {
    const { secondary } = theme.palette.text;

    return {
        key   : { flexBasis: '33.33%', flexShrink: 0 },
        value : { color: secondary }
    };
};
