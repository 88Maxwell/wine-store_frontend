import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import { MenuList, MenuItem } from '@material-ui/core';

import { muiStyles } from './navigation.style';


const Navigation = ({ classes }) => (
    <MenuList className={classes.menuList} component='nav'>
        <MenuItem className={classes.menuItem}>SomeItem-1</MenuItem>
        <MenuItem className={classes.menuItem}>SomeItem-2</MenuItem>
    </MenuList>
);

Navigation.propTypes = {
    classes : PropTypes.object.isRequired
};

export default withStyles(muiStyles)(Navigation);
