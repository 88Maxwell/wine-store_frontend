import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    Button,
    Divider,
    Typography,
    List,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails
} from '@material-ui/core';
import { Delete, Edit } from '@material-ui/icons';

import * as WineActions from '../../actions/wine';

import WineDialog from '../dialogs/WineDialog';
import ConfirmDialog from '../dialogs/ConfirmDialog';
import WineData from './WineData';

import { muiStyles } from './wine.styles';

class Wine extends Component {
    static propTypes = {
        data         : PropTypes.object.isRequired,
        classes      : PropTypes.object.isRequired,
        remove       : PropTypes.func.isRequired,
        handleExpand : PropTypes.func.isRequired,
        expanded     : PropTypes.bool.isRequired
    };

    state = {
        confirmRemoveDialog : false,
        wineDialog          : false
    };

    handleRemove = () => async evt => {
        evt.stopPropagation();
        evt.preventDefault();
        const { data, remove } = this.props;

        await remove(data.id);
        this.handleCloseDialog('confirmRemoveDialog');
    };

    handleOpenDialog = dialogName => evt => {
        evt.stopPropagation();
        this.setState({ [dialogName]: true });
    };

    handleCloseDialog = dialogName => evt => {
        if (evt) evt.stopPropagation();
        this.setState({ [dialogName]: false });
    };

    render() {
        const { classes, data, expanded, handleExpand } = this.props;
        const { confirmRemoveDialog, wineDialog } = this.state;

        return (
            <ExpansionPanel expanded={expanded} onChange={handleExpand}>
                <ExpansionPanelSummary className={classes.expansionSummary}>
                    <Grid container justify='space-between' alignItems='center'>
                        <Grid item>
                            <Typography>{data.name}</Typography>
                        </Grid>
                        <Grid item>
                            <Button onClick={this.handleOpenDialog('wineDialog')}>
                                <Edit />
                                { wineDialog ?
                                    <WineDialog
                                        data={data}
                                        dialogType='update'
                                        isOpen={wineDialog}
                                        handleClose={this.handleCloseDialog('wineDialog')}
                                    >
                                        Update information for your wine!
                                    </WineDialog>
                                    : null }
                            </Button>
                            <Button onClick={this.handleOpenDialog('confirmRemoveDialog')}>
                                <Delete />
                                { confirmRemoveDialog ?
                                    <ConfirmDialog
                                        title='Confirm remove Wine'
                                        isOpen={confirmRemoveDialog}
                                        handleClose={this.handleCloseDialog('confirmRemoveDialog')}
                                        handleConfirm={this.handleRemove()}
                                    >
                                        {`Are you shure to remove "${data.name}"`}
                                    </ConfirmDialog>
                                    : null }
                            </Button>
                        </Grid>
                    </Grid>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.expansionDetails}>
                    <List component='ul' className={classes.wineDataList}>
                        <Divider />
                        <WineData keys='Name'>{data.name}</WineData>
                        <WineData keys='Counry'>{data.country}</WineData>
                        <WineData keys='Age'>{`${data.age} year`}</WineData>
                        <WineData keys='Varietal'>{data.varietal}</WineData>
                    </List>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }
}

export default connect(
    null,
    { ...WineActions }
)(withStyles(muiStyles)(Wine));
