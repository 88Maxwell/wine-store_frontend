import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import {
    Paper,
    Grid,
    Typography
} from '@material-ui/core';

// import Navigation from '../widgets/Navigation';
import { muiStyles } from './main.styles';

class Main extends Component {
    static propTypes = {
        classes  : PropTypes.object.isRequired,
        children : PropTypes.node.isRequired
    };

    render() {
        const { classes } = this.props;

        return (
            <Grid
                container
                className={classes.container}
                direction='column'
                alignItems='center'
                justify='space-between'
            >
                <Grid
                    className={classes.item} item xs={12}
                    md={8}
                >
                    <Paper className={classes.headAndFoot}>
                        <Grid
                            container
                            alignItems='center'
                            justify='center'
                            // justify='space-between'
                        >
                            <Typography
                                component='h3'
                                variant='h3'
                                className={classes.logoTypography}
                            >
                                Wine Store
                            </Typography>
                            {/* <Navigation /> */}
                        </Grid>
                    </Paper>

                    <Grid>{this.props.children}</Grid>
                </Grid>

                <Grid
                    className={classes.item} item xs={12}
                    md={8}
                >
                    <Paper className={classes.headAndFoot} >
                        <Grid container justify='center'>
                            <Typography className={classes.footer}>Ⓒ 2019 Wine Store all rights reserved</Typography>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(muiStyles)(Main);
