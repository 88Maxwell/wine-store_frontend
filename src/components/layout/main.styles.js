import { red } from '@material-ui/core/colors';

export const muiStyles = theme => {
    const { unit } = theme.spacing;

    return {
        headAndFoot : {
            padding         : unit * 2,
            'margin-bottom' : unit,
            backgroundColor : red[50]
        },
        paper : {
            padding         : unit * 2,
            'margin-bottom' : unit
        },
        container      : { 'min-height': '100vh' },
        item           : { width: '100%', margin: unit },
        logoTypography : { 'font-family': 'Pacifico, cursive' },
        footer         : { 'font-family': 'Pacifico, cursive' }
    };
};
