export const muiStyles = theme => {
    const { unit } = theme.spacing;

    return ({
        paper     : { padding: unit * 2 },
        menuItem  : { display: 'inline-flex' },
        item      : { width: '100%' },
        addButton : { 'margin-bottom': unit * 2 },
        menuList  : { backgroundColor: 'red' }
    });
};
