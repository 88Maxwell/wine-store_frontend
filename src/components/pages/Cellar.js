import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { withStyles, Grid, Paper, Typography, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import * as WineActions from '../../actions/wine';

import Wine from '../widgets/Wine';
import Search from '../widgets/Search';
import WineDialog from '../dialogs/WineDialog';

import { muiStyles } from './cellar.styles';

class Cellar extends Component {
    static propTypes = {
        classes : PropTypes.object.isRequired,
        list    : PropTypes.func.isRequired,
        wines   : PropTypes.array.isRequired
    };

    state = {
        wineDialog : false,
        expandedId : null
    };

    async componentWillMount() {
        await this.props.list({});
    }

    handleExpand = expandedId => () =>
        this.setState(state => ({ expandedId: state.expandedId === expandedId ? null : expandedId }));

    handleOpenDialog = dialogName => () => this.setState({ [dialogName]: true });

    handleCloseDialog = dialogName => () => this.setState({ [dialogName]: false });

    renderWines = () => {
        const { wines } = this.props;
        const { expandedId } = this.state;

        return wines.length ? (
            wines.map(wine => (
                <Wine
                    expanded={expandedId === wine.id}
                    handleExpand={this.handleExpand(wine.id)}
                    key={wine.id}
                    data={wine}
                />
            ))
        ) : (
            <Typography>There no wines</Typography>
        );
    };

    render() {
        const { classes } = this.props;
        const { wineDialog } = this.state;

        return (
            <Grid container className={classes.container} spacing={8}>
                <Grid item xs={12}>
                    <Search />
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Grid
                            container
                            direction='column'
                            alignItems='center'
                        >
                            <Grid item>
                                <Fab
                                    size='small'
                                    color='primary'
                                    aria-label='Add'
                                    className={classes.addButton}
                                    onClick={this.handleOpenDialog('wineDialog')}
                                >
                                    <AddIcon />
                                </Fab>

                            </Grid>
                            <Grid item className={classes.item}>
                                {this.renderWines()}
                            </Grid>
                        </Grid>
                    </Paper>


                </Grid>
                <WineDialog
                    dialogType='create'
                    isOpen={wineDialog}
                    handleClose={this.handleCloseDialog('wineDialog')}
                >
                    Please enter information for you new wine in your collection !
                </WineDialog>
            </Grid>
        );
    }
}

export default connect(
    state => ({ wines: state.wines.wines }),
    { ...WineActions }
)(withStyles(muiStyles)(Cellar));
