import React from 'react';
import PropTypes from 'prop-types';

import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Button,
    Grid
} from '@material-ui/core';

export default class ConfirmDialog extends React.Component {
    static propTypes = {
        isOpen        : PropTypes.bool.isRequired,
        handleClose   : PropTypes.func.isRequired,
        handleConfirm : PropTypes.func.isRequired,
        title         : PropTypes.string.isRequired,
        children      : PropTypes.node.isRequired
    };

    render() {
        const {
            isOpen,
            title,
            children,
            handleClose,
            handleConfirm
        } = this.props;

        return (
            <Dialog
                open={isOpen}
                onClose={handleClose}
                aria-labelledby={`${title}-dialog`}
            >
                <form onSubmit={handleConfirm}>
                    <DialogTitle id={`${title}-dialog`}>
                        {title}
                    </DialogTitle>
                    <DialogContent>
                        <Grid container direction='column'>
                            <DialogContentText>
                                {children}
                            </DialogContentText>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color='secondary'>Cancel</Button>
                        <Button type='submit' color='primary'>Ok</Button>
                    </DialogActions>
                </form>
            </Dialog>
        );
    }
}
