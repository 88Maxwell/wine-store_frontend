import React from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Button,
    Divider,
    Grid
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import validate from '../../utils/validate';

import * as WineActions from '../../actions/wine';

import FILTERS from '../../../configs/Wine';

import Select from '../elements/Select';
import InputField from '../elements/InputField';

import { muiStyles } from './wineDialog.style';

class WineDialog extends React.Component {
    static propTypes = {
        data        : PropTypes.object,
        create      : PropTypes.func,
        update      : PropTypes.func,
        dialogType  : PropTypes.string.isRequired,
        classes     : PropTypes.object.isRequired,
        isOpen      : PropTypes.bool.isRequired,
        handleClose : PropTypes.func.isRequired,
        children    : PropTypes.node.isRequired
    };

    state = {};

    getInitialState = () => ({
        varietal : '',
        age      : '',
        country  : '',
        name     : '',
        errors   : {}
    });

    componentWillMount() {
        const { data } = this.props;

        this.setState(data ? { ...data, errors: {} } : this.getInitialState());
    }

    handleChange = param => event => {
        const { value } = event.target;
        const { data, errors, isValid } = validate({ [param]: value }, { [param]: this.wineValidationRules[param] });

        this.setState(state =>
            isValid
                ? {
                    [param] : data[param],
                    errors  : { ...state.errors, [param]: null }
                }
                : {
                    [param] : value,
                    errors  : { ...state.errors, [param]: errors[param] }
                }
        );
    };

    handleCreate = async evt => {
        evt.preventDefault();
        evt.stopPropagation();

        const { create } = this.props;
        const { age, varietal, country, name } = this.state;
        const wineData = { varietal, age, country, name };
        const validationResult = validate(wineData, this.wineValidationRules);

        if (validationResult.isValid) {
            await create(validationResult.data);
            this.handleCloseAndCleanState();
        } else {
            this.setState({ errors: validationResult.errors });
        }
    };

    handleUpdate = async evt => {
        evt.preventDefault();
        evt.stopPropagation();

        const { update, data } = this.props;
        const { age, varietal, country, name } = this.state;
        const wineData = { id: data.id, varietal, age, country, name };

        const validationResult = validate(wineData, this.wineValidationRules);

        if (validationResult.isValid) {
            await update(wineData);
            this.handleCloseAndCleanState();
        } else {
            this.setState({ errors: validationResult.errors });
        }
    };

    handleCloseAndCleanState = () => {
        const { handleClose } = this.props;

        this.setState(this.getInitialState());
        handleClose();
    };

    getPropsFromTypes = () => {
        const { dialogType, handleClose } = this.props;

        switch (dialogType) {
            case 'create':
                return {
                    onSubmit          : this.handleCreate,
                    onClose           : this.handleCloseAndCleanState,
                    submitButtonLabel : 'Create'
                };

            case 'update':
                return {
                    onSubmit          : this.handleUpdate,
                    onClose           : handleClose,
                    submitButtonLabel : 'Update'
                };

            default:
                break;
        }
    }

    wineValidationRules = {
        varietal : [ 'required' ],
        age      : [ 'required' ],
        country  : [ 'required' ],
        name     : [ 'required' ]
    };

    render() {
        const { classes, isOpen, children } = this.props;
        const { age, varietal, country, name, errors } = this.state;
        const { VARIETAL, COUNTRY, AGE } = FILTERS;
        const { onSubmit, submitButtonLabel, onClose } = this.getPropsFromTypes();

        return (
            <Dialog
                open={isOpen}
                onClose={onClose}
                aria-labelledby={`${submitButtonLabel}-wine-dialog`}
            >
                <form onSubmit={onSubmit}>
                    <DialogTitle id='create-wine-dialog'>Create Wine</DialogTitle>
                    <DialogContent>
                        <Grid container direction='column'>
                            <DialogContentText>
                                {children}
                            </DialogContentText>
                            <Divider />
                            <InputField
                                id='input-input'
                                elementStyle={classes.formControl}
                                required
                                value={name}
                                label='Name'
                                error={errors && errors.name}
                                handleChange={this.handleChange('name')}
                            />
                            <Select
                                id='select-varietal'
                                elementStyle={classes.formControl}
                                required
                                label='Varietal'
                                value={varietal}
                                error={errors && errors.varietal}
                                valueList={VARIETAL}
                                handleChange={this.handleChange('varietal')}
                            />
                            <Select
                                id='select-country'
                                elementStyle={classes.formControl}
                                required
                                label='Country'
                                error={errors && errors.country}
                                value={country}
                                valueList={COUNTRY}
                                handleChange={this.handleChange('country')}
                            />
                            <Select
                                id='select-age'
                                elementStyle={classes.formControl}
                                required
                                label='Age'
                                error={errors && errors.age}
                                value={`${age}`}
                                valueList={AGE}
                                handleChange={this.handleChange('age')}
                            />
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button type='button' onClick={onClose} color='secondary'>
                                Cancel
                        </Button>
                        <Button type='submit' color='primary'>
                            {submitButtonLabel}
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        );
    }
}

export default connect(
    null,
    { ...WineActions }
)(withStyles(muiStyles)(WineDialog));
