import ReactDOM from 'react-dom';
import React from 'react';
import { Provider } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';

import theme from './theme.style';
import './index.css';
import App from './components';
import store from './store';


ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <Provider store={store} >
            <App />
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('root'));
